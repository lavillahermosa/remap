# Chemins

A processwire module for research mapping.

## STATUS
This repo is currently in pause mode, waiting for other projects using chemins to end before merging changes from them. The TODO list is the only thing that will change in the next few weeks.

## TODO
* generic templates creation at install
* multiple instances possible in the same Processwire. 
** admin page to create new instances
** pages creation at instance creation
** link a user to the instance


* dictionary for translation from js
* multi-user realtime thing (better than turn-by-turn)
