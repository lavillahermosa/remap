<?php
namespace ProcessWire;

function getNodes(){
    $nodes = [];
    $lng = wire('languages')->getLanguage();
    $nodesContainer = wire('pages')->get('/nodes/');
    $nodePages = wire('pages')->find('parent='.$nodesContainer->children);
    foreach($nodePages as $nodePage){
        $node = ['id' => $nodePage->id, 'parent' => $nodePage->parent->id, 
            'title' => $nodePage->title->getLanguageValue($lng),
            'x' => $nodePage->x,
            'y' => $nodePage->y
        ];
        $nodes[] = $node;
    }
    return $nodes;



}
function getEdges(){
    $edges = [];
    $lng = wire('languages')->getLanguage();
    $edgesContainer = wire('pages')->get('/edges/');
    $edgePages = wire('pages')->find('parent='.$edgesContainer->children);
    foreach($edgePages as $edgePage){
        $edge = ['id' => $edgePage->id, 'parent' => $edgePage->parent->id, 
            'title' => $edgePage->title->getLanguageValue($lng),
            'nodeA' => $edgePage->node_a->id,
            'nodeB' => $edgePage->node_b->id
        ];
        $edges[] = $edge;
    }
    return $edges;



}


function getNodeTypes(){
    $nodeTypes = [];

    $nodesContainer = wire('pages')->get('/nodes/');
    $defaultShape = wire('pages')->get('/settings/shapes/')->child();
    $defaultBorder = wire('pages')->get('/settings/border-types/')->child();
    $lng = wire('languages')->getLanguage();

    foreach($nodesContainer->children as $parentNode){

        $childTemplates = $parentNode->template->childTemplates();
        
        
        $border = ($parentNode->border_type->id != 0)?
            $parentNode->border_type->name:$defaultBorder->name;
        $shape = ($parentNode->shape_type->id != 0)?
            $parentNode->shape_type->name:$defaultShape->name;
        
        $nodeTypes[$parentNode->id] = [
            'parent' => $parentNode->id, 
            'name' => $parentNode->singular_title->getLanguageValue($lng),
            'template' => $childTemplates->first()->name,
            'border' => $border,
            'shape' => $shape
        ];
        
    }
    return $nodeTypes;
}
function getEdgeTypes(){
    $edgeTypes = [];

    $edgesContainer = wire('pages')->get('/edges/');
    $defaultBorder = wire('pages')->get('/settings/border-types/')->child();
    $lng = wire('languages')->getLanguage();

    foreach($edgesContainer->children as $parentEdge){

        $childTemplates = $parentEdge->template->childTemplates();
        
        $border = ($parentEdge->border_type->id != 0)?
            $parentEdge->border_type->name:$defaultBorder->name;
        
        
        $edgeTypes[$parentEdge->id] = [
            'parent' => $parentEdge->id, 
            'name' => $parentEdge->singular_title->getLanguageValue($lng),
            'template' => $childTemplates->first()->name,
            'border' => $border
        ];
        
    }
    return $edgeTypes;
}


function saveNode($title, $parentId, $template, $x, $y){
    $page = wire('pages')->newPage([
        'template' => $template,
        'parent' => $parentId,
        'title' => $title,
        'x' => $x,
        'y' => $y

    ]);
    $page->save();

    return $page->id;
}
function saveEdge($title, $parentId, $template, $nodeAId, $nodeBId){
    $page = wire('pages')->newPage([
        'template' => $template,
        'parent' => $parentId,
        'title' => $title,
        'node_a' => $nodeAId,
        'node_b' => $nodeBId

    ]);
    $page->save();

    return $page->id;
}

function updatePage($id, $data){
    $page = wire('pages')->get($id);
    foreach($data as $field => $value){
        $page->set($field, $value);
    }
    $page->save();
    return true;
}
