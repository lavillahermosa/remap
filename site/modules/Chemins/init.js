import Router from './js/Router.js';
import Interface from './js/Interface.js';

const router =  Router();
const itf = Interface({
    getNodeTypes:router.getNodeTypes,
    getEdgeTypes:router.getEdgeTypes,
    getNodes:router.getNodes,
    saveNode:router.saveNode,
    updateNode:router.updatePage,
    getEdges:router.getEdges,
    saveEdge:router.saveEdge,
    updateEdge:router.updatePage
});