import { drawFromPoints, computeLine, showPoints } from "./Draw.js";


const Edge = ($container, {
    id = null, 
    title = null,
    edgeType = null,
    nodeA = null,
    nodeB = null,
    onEdgeClick = null
    } = {}) => {

        const snapElem = Snap();
        const $edge = document.createElement('div');
        const $title = document.createElement('h3');

        

        $edge.classList.add('edge');
        const strokeWidth = 1, definition = 12, canvasSize = 50;
        let width, height, x, y, angle;
        let shapePoints, shape;

        const init = () => {
            $edge.appendChild($title);
            $title.innerText = title;
            $title.classList.add('action');
            $title.setAttribute('data-action', 'renameEdge');
            $title.setAttribute('data-action-type', 'dblclick');
            $edge.setAttribute('data-id', id);
            $edge.appendChild(snapElem.node);
            $container.appendChild($edge);
            initEvents();
            update();

            
        };

        const initEvents = () => {
            $edge.addEventListener('click', () => onEdgeClick?.(id));
        };

        const update = () => {
            let pos = computePosition();
            setPosition(pos);
            snapElem.attr({
                width:width,
                height:height
            });
            console.log('position NODE A', nodeA.get('x'));
            shapePoints = computeShapePoints();
            if(shape != null){
                shape.remove();

            }
            shape = drawFromPoints(snapElem, shapePoints, edgeType.border, false);
            shape.attr({
                display:'inline',
                stroke:"#000",
                fill:'transparent',
                strokeWidth:strokeWidth
            });
        };
        const reverse = () => {
            const nodeTmp = nodeA;
            nodeA = nodeB;
            nodeB = nodeTmp;
            update();
        };

        const setPosition = ({
            newX = x, newY = y, 
            newWidth = width, newHeight = height, 
            newAngle = angle
        } = {}) => {
            x = newX;
            y = newY;
            width = newWidth;
            height = newHeight;
            angle = newAngle;

            $edge.style.left = `${x}px`;
            $edge.style.top = `${y}px`;
            $edge.style.width = `${width}px`;
            $edge.style.height = `${height}px`;
            $edge.style.transform = `rotate(${(angle/Math.PI)*180}deg)`;
            console.log($edge);

        };

        const set = (field, value) => {
            switch(field){
                case 'title':
                    title = value;
                    $title.innerText = value;
                    break;
                case 'id':
                    id = value;
                    $edge.setAttribute('data-id', id);
                    break;

            }
        };

        const get = (field) => {
            switch(field){
                case 'x':
                    return x;
                case 'y':
                    return y;
                case 'id':
                    return id;
                case 'title':
                    return title;
                case 'nodeAId':
                    return nodeA.get('id');
                case 'nodeBId':
                    return nodeB.get('id');
            }
        }

        const computeShapePoints = () => {
            const startPoint = nodeA.getBorderPoint(angle);
            const endPoint = nodeB.getBorderPoint(angle-Math.PI);
            const startDist = Math.sqrt((Math.pow(startPoint.x-nodeA.get('width')/2,2))+(Math.pow(startPoint.y-nodeA.get('height')/2,2)));
            const endDist =  Math.sqrt((Math.pow(endPoint.x-nodeB.get('width')/2,2))+(Math.pow(endPoint.y-nodeB.get('height')/2,2)));
           
            //la ligne est rectiligne par défaut

            return computeLine(startDist, height/2, width-endDist, height/2, definition);
            
        };

        const computePosition = () => {
            //va falloir définir les deux points du segment
            //en absolu en fonction des tracés des nodes.

            //on part de la position en xy du milieu de chacun des nodes
            const middlePosNodeA = {
                'x':nodeA.get('x') + nodeA.get('width')/2,
                'y':nodeA.get('y') + nodeA.get('height')/2
            };
            const middlePosNodeB = {
                'x':nodeB.get('x') + nodeB.get('width')/2,
                'y':nodeB.get('y') + nodeB.get('height')/2
            };


            const angle = Math.atan2(middlePosNodeB.y-middlePosNodeA.y, middlePosNodeB.x-middlePosNodeA.x);
            const distance = Math.sqrt((Math.pow(middlePosNodeB.x-middlePosNodeA.x,2))+(Math.pow(middlePosNodeB.y-middlePosNodeA.y,2)));
            return {
                newX:middlePosNodeA.x, 
                newY:middlePosNodeA.y - canvasSize/2,
                newWidth:distance,
                newHeight:canvasSize,
                newAngle:angle
            };



        };

        init();
        return {$edge, get, set, update, reverse};

};

export default Edge;