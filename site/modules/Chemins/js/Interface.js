import Node from "./Node.js";
import Edge from "./Edge.js";

const Interface = ({
    getNodeTypes = null,
    getEdgeTypes = null,
    getNodes = null,
    saveNode = null,
    updateNode = null,
    getEdges = null,
    saveEdge = null,
    updateEdge = null
    } = {}) => {


    let $scene, $addMenuNode, $addMenuEdge, $spaceArrows;

    let keyPressed = null;

    let dataNodeTypes, dataEdgeTypes;

    const nodes = [], edges = [];
    const nodesById = {}, edgesById = {}, edgesByNodeId = {};
    
    let stopEvents = false;
    
    const graphSize = {'width':0, 'height':0};

    //how much empty space on the bottom and the left 
    const graphMargin = 100;

    const init = async() => {
        $scene = document.querySelector('section.remap-container');
        dataNodeTypes = await getNodeTypes?.();
        dataEdgeTypes = await getEdgeTypes?.();
        $addMenuNode = buildAddMenu(dataNodeTypes, "add node", "addNode");
        $addMenuEdge = buildAddMenu(dataEdgeTypes, "add relation", "addEdge");
        $spaceArrows = getSpaceArrows();
        const dataNodes = await getNodes?.();
        buildNodes(dataNodes);
        const dataEdges = await getEdges?.();
        buildEdges(dataEdges);

        initEvents($scene);
        
        document.addEventListener('scroll', toggleSpaceArrows);
        document.addEventListener('keydown', e => {keyPressed = e.key;});
        document.addEventListener('keyup', e => {keyPressed = null});
        
        setSceneSize();
        toggleSpaceArrows();
    }

    const initEvents = ($dom) => {
        const $buttons = [...$dom.querySelectorAll('.action')];
        if($dom.classList.contains('action')){
            $buttons.push($dom);
        }
        $buttons.forEach($button => {
            const action = $button.getAttribute('data-action');
            const actionType = $button.getAttribute('data-action-type') ?? 'click';
            

            $button.addEventListener(actionType, e => {
                
                if(stopEvents) return;
                
                actions[action]?.(e);
            });


        });
    }

    const pauseEvents = () => {
        stopEvents = true;
    };
    const startEvents = () => {
        stopEvents = false;
    };

    const actions = {
        'addSpace':(e) => {
            const $button = e.target;
            const direction = $button.getAttribute('data-direction');
            switch(direction){
                case 'up':
                    nodes.forEach(node => {
                        node.set('position', {'x':node.get('x'), 'y':node.get('y') + 100});
                    });
                    setSceneSize();
                    break;
                case 'left':
                    nodes.forEach(node => {
                        node.set('position', {'x':node.get('x') + 100, 'y':node.get('y')});
                    });
                    setSceneSize();
                    break;
                case 'right':
                    $scene.style.width = $scene.offsetWidth + 100 + 'px';
                    window.scrollBy(100, 0);
                    break;
                case 'bottom':
                    $scene.style.height = $scene.offsetHeight + 100 + 'px';
                    window.scrollBy(0, 100);
                    break;
                
            }
            console.log(e.target.getAttribute('data-direction'));

        },

        'showAddMenuNode': (e) => {
            if(e.target.getAttribute('data-action') != 'showAddMenuNode' ||
            $addMenuNode.classList.contains('active') || 
            stopEvents) return;
           
            $addMenuNode.classList.toggle('active');
            $addMenuNode.style.left = `${e.pageX - window.scrollX}px`;
            $addMenuNode.style.top = `${e.pageY - window.scrollY}px`;
            window.setTimeout(() => {
                document.body.addEventListener('click', e => {
                    console.log('hide');
                    $addMenuNode.classList.remove('active');
                }, {once:true});
            }, 10);

        },
        'renameNode': (e) => {
            console.log('rename', e);
            const nodeId = e.target.parentElement.getAttribute('data-id');
            renameNode(nodesById[nodeId]);
            

        },
        
        'addNode': async(e) => {
            //e.stopPropagation();
            console.log(e);
            $addMenuNode.classList.remove('active');
            const $button = e.target;
            const parentId = $button.getAttribute('data-parent');
            const nodeType = dataNodeTypes[parentId];
            console.log(nodeType);
            const x = e.pageX;
            const y = e.pageY;

            const title = await multiPrompt("enter node title");
            if(title == '') return;
            const node = createNode(title, nodeType, x, y);
            const nodeId = await saveNode?.(title, parentId, nodeType.template, x, y);
            node.set('id', nodeId); 
            nodes.push(node);
            nodesById[node.get('id')] = node;
            initEvents(node.$node);
            
            
        },
        'showAddMenuEdge': (e) => {
            if($addMenuEdge.classList.contains('active')){
                return;
            }
            const $button = e.target;
            const nodeId = $button.closest('.node').getAttribute('data-id');
            $addMenuEdge.setAttribute('data-id', nodeId);
            $addMenuEdge.classList.add('active');
            $addMenuEdge.style.left = `${e.pageX - window.scrollX}px`;
            $addMenuEdge.style.top = `${e.pageY - window.scrollY}px`;
            window.setTimeout(() => {
                document.body.addEventListener('click', e => {
                    console.log('hide');
                    $addMenuEdge.classList.remove('active');
                }, {once:true});
            }, 10);
         
        },
        'renameEdge': (e) => {
            console.log('rename', e);
            const edgeId = e.target.parentElement.getAttribute('data-id');
            renameEdge(edgesById[edgeId]);
            

        },
        'addEdge': (e) => {
            const nodeId = $addMenuEdge.getAttribute('data-id');
            const $button = e.target;
            const parentId = $button.getAttribute('data-parent');
            const edgeType = dataEdgeTypes[parentId];
            const nodeA = nodesById[nodeId];
            
            nodes.forEach(node => {
                node.$node.classList.add('selectable');
            });

            window.setTimeout(() => {
                document.body.addEventListener('click', async e => {
                    console.log('create link');
                    const $button = e.target;
                    const $nodeB = $button.closest('.node');
                    console.log('nodeB', $nodeB);
                    

                    if($nodeB != null && 
                        $nodeB.getAttribute('data-id') != nodeId){
                        //^TODO: also if the edge doesnt exist between the two nodes;
                        const nodeB = nodesById[$nodeB.getAttribute('data-id')];
                        
                        let title = await multiPrompt('enter edge title');
                        if(title == '')
                            title = `${nodeA.get('title')}-${nodeB.get('title')}`;
                        
                        
                        nodes.forEach(node => {
                            node.$node.classList.remove('selectable');
                        });
                          


                        const edge = createEdge(edgeType, nodeA, nodeB, title, null);


                        const edgeId = await saveEdge?.(title, 
                            parentId, edgeType.template, nodeA.get('id'), nodeB.get('id'));
                        
                        edge.set('id', edgeId); 
                        edges.push(edge);
                        edgesById[edgeId] = edge;

                        if(edgesByNodeId[nodeA.get('id')] == undefined){
                            edgesByNodeId[nodeA.get('id')] = [edge];
                        }else{
                            edgesByNodeId[nodeA.get('id')].push(edge);
                        }
                        if(edgesByNodeId[nodeB.get('id')] == undefined){
                            edgesByNodeId[nodeB.get('id')] = [edge];
                        }else{
                            edgesByNodeId[nodeB.get('id')].push(edge);
                        }
                        
                    }
                }, {once:true});
            }, 10);


        }

    };

    const getSpaceArrows = () => {
        return {
            'up':$scene.querySelector('.space-arrow.arrow-top'),
            'left':$scene.querySelector('.space-arrow.arrow-left'),
            'right':$scene.querySelector('.space-arrow.arrow-right'),
            'bottom':$scene.querySelector('.space-arrow.arrow-bottom')   
        }
    };

    const toggleSpaceArrows = () => {
        
        if(window.scrollY == 0){
            $spaceArrows.up.classList.add('active');
        }else{
            $spaceArrows.up.classList.remove('active');
        }
        if(window.scrollY + window.innerHeight >= document.body.scrollHeight){
            $spaceArrows.bottom.classList.add('active');
        }else{
            $spaceArrows.bottom.classList.remove('active');
        }
        if(window.scrollX == 0){
            $spaceArrows.left.classList.add('active');
        }else{
            $spaceArrows.left.classList.remove('active');
        }
        
        if(window.scrollX + window.innerWidth >= document.body.scrollWidth){

            $spaceArrows.right.classList.add('active');
        }else{
            $spaceArrows.right.classList.remove('active');
        }
    };

    const setSceneSize = () => {
        const sceneWidth = (window.screen.width > graphSize.width + graphMargin)?window.screen.width:graphSize.width + graphMargin;
        const sceneHeight = (window.screen.height > graphSize.height + graphMargin)?window.screen.height:graphSize.height + graphMargin;
        $scene.style.width = sceneWidth+'px';
        $scene.style.height = sceneHeight+'px';
    }

    const createEdge = (edgeType, nodeA, nodeB, title, id = null) => {
        const edge = Edge($scene, {id:id, title:title, nodeA:nodeA, nodeB:nodeB, edgeType:edgeType, onEdgeClick:edgeEvent});
        return edge;
    };

    const createNode = (title, nodeType, x, y, id = null) => {
        const node = Node($scene, {id:id, title:title, x:x, y:y, 
            nodeType:nodeType, onNodeEventStart:null, 
            onNodeEventStop:startEvents, onNodeMoved:updateNode,
            onNodeMove:(nodeId) => {
                const linkedEdges = edgesByNodeId[nodeId];
                if(linkedEdges == undefined)
                    return;

                linkedEdges.forEach(linkedEdge => {
                    linkedEdge.update();
                });
                console.log(linkedEdges);

            }
        });

        if(node.get('x') + node.get('width') > graphSize.width){
            graphSize.width = node.get('x') + node.get('width');
        }
        if(node.get('y') + node.get('height') > graphSize.height){
            graphSize.height = node.get('y') + node.get('height');
        }

        return node;
    };
   

    const multiPrompt = (message = null, _default = null) => {
        return new Promise((resolve, reject) => {
            const $modal = document.createElement('div');
            $modal.classList.add('modal');
            $modal.innerHTML = (
                `<div>
                    <h3>${message?message:'insert text below'}</h3>
                    <textarea>${_default?_default:''}</textarea>
                </div>`);
            
            document.body.classList.add('prompted');
            const $field = $modal.querySelector('textarea');


            $scene.append($modal);

            $field.focus();


            $field.addEventListener('focusout', e => {
                $modal.parentElement.removeChild($modal);
                resolve($field.value);
            });


        });


    };
    
    const edgeEvent = (edgeId) => {
        //console.log(keyPressed);
        const edge = edgesById[edgeId];
        if(keyPressed == 'Control'){
            edge.reverse();
            updateEdge(edge.get('id'), {'node_a':edge.get('nodeAId'), 'node_b':edge.get('nodeBId')});
        }
    };
    
    const renameNode = async(node) => {
        const title = await multiPrompt("enter new title", node.get('title'));
        if(title == '') return;
        
        node.set('title', title);
        updateNode(node.get('id'), {'title':node.get('title')});

    };

    const renameEdge = async(edge) => {
        console.log(edge);
        const title = await multiPrompt("enter new title", edge.get('title'));
        if(title == '') return;
        
        edge.set('title', title);
        updateEdge(edge.get('id'), {'title':edge.get('title')});
        
    }

    const buildNodes = (dataNodes) => {
        const graphSize = {width:0, height:0};
        dataNodes.forEach(dataNode => {
            const node = createNode(dataNode.title, dataNodeTypes[dataNode.parent], dataNode.x, dataNode.y, dataNode.id);
            nodes.push(node);
            
            nodesById[node.get('id')] = node;
        });
        return graphSize;
    };
    const buildEdges = (dataEdges) => {
        dataEdges.forEach(dataEdge => {
            const edge = createEdge(dataEdgeTypes[dataEdge.parent], 
                nodesById[dataEdge.nodeA], nodesById[dataEdge.nodeB], dataEdge.title, dataEdge.id);
            edges.push(edge);
            edgesById[edge.get('id')] = edge;
            if(edgesByNodeId[dataEdge.nodeA] == undefined){
                edgesByNodeId[dataEdge.nodeA] = [edge];
            }else{
                edgesByNodeId[dataEdge.nodeA].push(edge);
            }
            if(edgesByNodeId[dataEdge.nodeB] == undefined){
                edgesByNodeId[dataEdge.nodeB] = [edge];
            }else{
                edgesByNodeId[dataEdge.nodeB].push(edge);
            }
        });
    };
    
    const buildAddMenu = (types, headerText, action) => {
        const $addMenu = document.createElement('ul');
        $addMenu.classList.add('ctx-menu', 'menu-add');
        const $menuHead = document.createElement('li');
        $menuHead.classList.add('menu-head');
        $menuHead.innerText = headerText;
        $addMenu.appendChild($menuHead);
        Object.values(types).forEach(type => {
            const $item = document.createElement('li');
            $item.innerText = type.name;
            $item.classList.add('menu-item');
            $item.setAttribute('data-parent', type.parent);
            $item.setAttribute('data-template', type.template);
            $item.classList.add('action');
            $item.setAttribute('data-action', action);
            
            $addMenu.appendChild($item);
        });
        $scene.appendChild($addMenu);
        return $addMenu;
    };


    init();

};

export default Interface;