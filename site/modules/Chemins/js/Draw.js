const computeLine = (x1, y1, x2, y2, definition) => {
    definition = 110/definition;
    const mainPoints = [
        {x:x1,y:y1},
        {x:x2,y:y2}
    ];
    return fragmentSegments(mainPoints, definition);

};


const computeLosange = (x1, x2, y1, y2, definition) => {
    definition = 110/definition;
    const mainPoints = [
        {x:x1,y:y1+(y2-y1)/2},
        {x:x1+(x2-x1)/2,y:y1},
        {x:x2,y:y1+(y2-y1)/2},
        {x:x1+(x2-x1)/2,y:y2},
        {x:x1,y:y1+(y2-y1)/2}
    ];
    return fragmentSegments(mainPoints, definition);
}

const computeRectangle = (x1, y1, x2, y2, definition) => {
    definition = 110/definition;
    const mainPoints = [
        {'x':x1,'y':y1},
        {'x':x2, 'y':y1},
        {'x':x2, 'y':y2},
        {'x':x1, 'y':y2},
        {'x':x1,'y':y1}
        
    ];
    return fragmentSegments(mainPoints, definition);

};


const fragmentSegments = (mainPoints, definition) => {
    const points = [];
    for(let i = 0; i < mainPoints.length-1; i++){
        points.push(mainPoints[i]);
        const pointA = mainPoints[i];
        const pointB = mainPoints[i+1];

        const distance = Math.sqrt((Math.pow(pointB.x-pointA.x,2))+(Math.pow(pointB.y-pointA.y,2)));
        const theta = Math.atan2(pointB.y-pointA.y, pointB.x-pointA.x);
        const steps = distance / definition;
        definition = distance / Math.floor(steps);
        for(let i = 1; i < steps; i++){
            const newPoint = {
                'x':pointA.x + Math.cos(theta) * (i * definition),
                'y':pointA.y + Math.sin(theta) * (i * definition)
            }
            points.push(newPoint);
        }
        
    }

    return points;
};


const computeEllipse = (x0, y0, width, height, definition) => {

    const computeDpt = (r1, r2, theta ) => {
        let dp=0.0;
    
        let dpt_sin = Math.pow(r1*Math.sin(theta), 2.0);
        let dpt_cos = Math.pow( r2*Math.cos(theta), 2.0);
        dp = Math.sqrt(dpt_sin + dpt_cos);
    
        return dp;
    };
    

    const r1 = width/2;
    const r2 = height/2;

    let theta = 0.0;
    const twoPi = Math.PI*2.0;
    const deltaTheta = 0.0001;
    const numIntegrals = Math.round(twoPi/deltaTheta);
    let circ=0.0;
    let dpt=0.0;

    const points = [];


    /* integrate over the elipse to get the circumference */
    for(let i=0; i < numIntegrals; i++ ) {
        theta += i*deltaTheta;
        dpt = computeDpt( r1, r2, theta);
        circ += dpt;
    }
    
    const n=Math.round(circ*0.000001)*definition;
    let nextPoint = 0;
    let run = 0.0;
    theta = 0.0;

    for(let i=0; i < numIntegrals; i++ ) {
        theta += deltaTheta;
        let subIntegral = n*run/circ;
        if(subIntegral >= nextPoint ) {
            let x = r1 * Math.cos(theta);
            let y = r2 * Math.sin(theta);
            
            points.push({'x':x0+x, 'y':y0+y});

            nextPoint++;
        }
        run += computeDpt(r1, r2, theta);
    }



    return points;
};


const drawFromPoints = (snapElem, points, strokeType, close = false) => {
   
    let pathString;


    switch(strokeType){
        case 'straight':
            pathString = makeStraightPath(points, close);
            break;
        case 'curly':
            pathString = makeCurlyPath(points, close);
            break;
        default:
            pathString = makeStraightPath(points, close);

    }

    return snapElem.path(pathString);
    
}

const showPoints = (snapElem, points) => {
    points.forEach(point => {
        snapElem.circle(point.x, point.y, 2);
    });

};

const makeStraightPath = (points, close) => {
    let pathString = `M${points[0].x},${points[0].y}`;
    for(let i = 1; i < points.length; i++){
        pathString += `L${points[i].x},${points[i].y}`;
    }
    if(close){
        pathString += `L${points[0].x},${points[0].y}Z`;
    }
    return pathString;
    
};

const makeCurlyPath = (points, close) => {
    let pathString = `M${points[0].x},${points[0].y}`;
    let prevPoint = points[0];
    if(close){
        points.push(prevPoint);
    }
    for(let i = 1; i < points.length; i++){
        let point = points[i];
        let theta = Math.atan2(point.y-prevPoint.y, point.x-prevPoint.x);
        const distance = Math.sqrt((Math.pow(point.x-prevPoint.x,2))+(Math.pow(point.y-prevPoint.y,2)))*2.5;
        let cp1Theta = theta - Math.PI/5;
        let cp2Theta = theta + Math.PI + Math.PI/5;

        const cp1 = {
            x:prevPoint.x+Math.cos(cp1Theta) * distance,
            y:prevPoint.y+Math.sin(cp1Theta) * distance
        };
        const cp2 = {
            x:point.x+Math.cos(cp2Theta) * distance,
            y:point.y+Math.sin(cp2Theta) * distance
        }
        
        //compute 
        pathString += `C${cp1.x} ${cp1.y}, ${cp2.x} ${cp2.y}, ${point.x} ${point.y}`;
        prevPoint = point;
    }
    return pathString;
};


export {drawFromPoints, computeEllipse, computeLosange, computeLine, computeRectangle, showPoints};