import { drawFromPoints, computeEllipse, computeLosange, computeRectangle, showPoints } from "./Draw.js";

const Node = ($container, {
    title = null, x = 0, y = 0, id = null, 
    nodeType = null,
    onNodeEventStart = null,
    onNodeEventStop = null,
    onNodeMoved = null,
    onNodeMove = null} = {}) => {
    const snapElem = Snap();
    const $node = document.createElement('div');
    $node.classList.add('node');
    const $title = document.createElement('h3');
    const strokeWidth = 1, definition = 12, margin = 10;
    let width, height;
    
    //shapeB is a path only used for computation (hack to avoid bug in intersection test)
    let shapePoints, shape, shapeB;
    
    const computeShape = () => {
        $node.style.width = 'auto';
        $node.style.height = 'auto';

        width = $title.getBoundingClientRect().width + margin * 2;
        height = $title.getBoundingClientRect().height + margin * 2;

        $node.style.width = `${width}px`;
        $node.style.height = `${height}px`;

        snapElem.attr({
            width:width,
            height:height
        });

        shapePoints = computeShapePoints(nodeType.shape);
        
        shape = drawFromPoints(snapElem, shapePoints, nodeType.border, true);
        if(nodeType.border == 'straight'){
            const  shapePointsB = computeShapePoints(nodeType.shape, 10);
            shapeB = drawFromPoints(snapElem, shapePointsB, nodeType.border, true);

        }else{
            shapeB = shape;
        }
        shapeB.attr({
            display:'none'
        });

        shape.attr({
            display:'inline',
            fill:"#FFF",
            stroke:"#000",
            strokeWidth:strokeWidth
        });

    };

    const init = () => {
        console.log(snapElem);
        $title.classList.add('node-title','action');
        
        $title.setAttribute('data-action', 'renameNode');
        $title.setAttribute('data-action-type', 'dblclick');
        $title.innerText = title;
        
        $node.appendChild(snapElem.node);
        $node.appendChild($title);
        $node.setAttribute('data-id', id);
        $container.appendChild($node);
        
        console.log(nodeType);
        setPosition(x, y);
        
        computeShape();

        //showPoints(snapElem, shapePoints);
        buildMenu();
        initEvents();
        
    };
    const initEvents = () => {
        let mousePosX, mousePosY;
        const moveNode = (e) => {
            const deltaX = mousePosX - e.pageX;
            const deltaY = mousePosY - e.pageY;
            mousePosX = e.pageX;
            mousePosY = e.pageY;
            setPosition(x-deltaX, y-deltaY);
            onNodeMove?.(id);
            
        }

        $node.addEventListener('mousedown', e => {
            
            if(e.target != $title) return;
            mousePosX = e.pageX;
            mousePosY = e.pageY;
            $node.classList.add('dragging');
            console.log(e);
            onNodeEventStart?.();
            document.addEventListener('mousemove', moveNode);
            document.body.addEventListener('mouseup', e => {
                document.removeEventListener('mousemove', moveNode);
                $node.classList.remove('dragging');
                onNodeMoved?.(id, {'x':x, 'y':y});
                window.setTimeout(onNodeEventStop, 500);
            }, {once:true});
        });

        $node.addEventListener('mouseenter', e => {
            //show contextual menu
        });

        
        
    };

    const getBorderPoint = (angle) => {
        const limitPoint = {
            x:(width/2) + Math.cos(angle) * 200,
            y:(height/2) + Math.sin(angle) * 200
        };
        const intersectionPath = snapElem.path(`M${width/2}, ${height/2} L${limitPoint.x}, ${limitPoint.y}`);
        intersectionPath.attr({
            display:'none'
        });

        const intersects = Snap.path.intersection(shapeB, intersectionPath);
        console.log(intersects);
        const borderPoint = {
            'x':intersects[intersects.length - 1].x,
            'y':intersects[intersects.length - 1].y
        };
        

        return borderPoint;

    };

    const buildMenu = () => {
        const $menu = document.createElement('ul');
        $menu.classList.add('ctx-menu');
        $menu.innerHTML = `<li class="action icon link" data-action="showAddMenuEdge"></li>`;
        $node.appendChild($menu);
    };

    const set = (field, value) => {
        switch(field){
            case 'id':
                id = value;
                $node.setAttribute('data-id', id);
                break;
            case 'title':
                title = value;
                $title.innerText = title;
                computeShape();
                break;
            case 'position':
                setPosition(value.x, value.y);
                onNodeMove?.(id);
                onNodeMoved?.(id, {'x':x, 'y':y});
                break;
        }
    };

    const get = (field) => {
        switch(field){
            case 'x':
                return x;
            case 'y':
                return y;
            case 'id':
                return id;
            case 'title':
                return title;
            case 'width':
                return width;
            case 'height':
                return height;
        }
    };

    const setPosition = (newX, newY) => {
        x = newX;
        y = newY;
        $node.style.left = `${x}px`;
        $node.style.top = `${y}px`;
    };


    const computeShapePoints = (shape, def = definition) => {
          switch(shape){
            case 'ellipse':
                return computeEllipse(width/2, height/2, 
                    width - margin * 2 - strokeWidth*2, height - margin * 2 - strokeWidth*2, def);
            case 'losange':
                return computeLosange(margin, width-margin, margin, height - margin, def);
            case 'rectangle':
                return computeRectangle(margin, margin, width - margin, height-margin);
                
        }
        
    };

    
    init();

    return {id, title, width, height, $node, set, get, nodeType, getBorderPoint};

};

export default Node;