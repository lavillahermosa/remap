<?php
namespace ProcessWire;

if($config->ajax){
    $content = trim(file_get_contents("php://input"));
    $request = json_decode($content, true);
    $output = [];
    if(!isset($request['action'])){
        exit();
    }
    switch($request['action']){
        case 'getNodes':
            $output = getNodes();
            break;
        case 'getNodeTypes':
            $output = getNodeTypes();
            break;
        case 'getEdgeTypes':
            $output = getEdgeTypes();
            break;
        case 'saveNode':
            $output = saveNode($request['title'], $request['parentId'], $request['template'], $request['x'], $request['y']);
            break;
        case 'updatePage':
            $output = updatePage($request['id'], $request['data']);
            break;
        case 'getEdges':
            $output = getEdges();
            break;
        case 'saveEdge':
            $output = saveEdge($request['title'], $request['parentId'], $request['template'], $request['nodeAId'], $request['nodeBId']);
            break;
    }

    echo json_encode($output);

}else{
?>

<section class="chemins-container action" data-action="showAddMenuNode">
    <section class="map-ui">
        <div class="action space-arrow arrow-top" data-direction="up" data-action="addSpace"></div>
        <div class="action space-arrow arrow-right" data-direction="right" data-action="addSpace"></div>
        <div class="action space-arrow arrow-bottom" data-direction="bottom" data-action="addSpace"></div>
        <div class="action space-arrow arrow-left" data-direction="left" data-action="addSpace"></div>

    </section>
</section>
<script type="module" src="<?=$this->config->urls->Chemins?>init.js"></script>
<?php
}
?>