<?php

/**
 * ProcessHello.info.php
 * 
 * Return information about this module.
 *
 * If preferred, you can use a getModuleInfo() method in your module file, 
 * or you can use a ModuleName.info.json file (if you prefer JSON definition). 
 *
 */

$info = array(

	
	'title' => 'Chemins', 

	
	'summary' => 'A processwire module for mapping research projects.', 

	
	'version' => 1, 

	
	'author' => 'Lionel Maes', 

	// Icon to accompany this module (optional), uses font-awesome icon names, minus the "fa-" part
	'icon' => 'location-arrow', 

	// URL to more info: change to your full modules.processwire.com URL (if available), or something else if you prefer
	//'href' => 'http://modules.processwire.com/', 

	
	'permission' => 'chemins', 

	'permissions' => array(
		'chemins' => 'Run the Chemins module'
	), 
	
	// page that you want created to execute this module
	'page' => array(
		'name' => 'chemins',
		'parent' => '', 
		'title' => 'Chemins'
	)

	// optional extra navigation that appears in admin
    // if you change this, you'll need to a Modules > Refresh to see changes
    /*
	'nav' => array(
		array(
			'url' => '', 
			'label' => 'Hello',  
			'icon' => 'smile-o', 
		), 
		array(
			'url' => 'something/', 
			'label' => 'Something', 
			'icon' => 'beer', 
		),
	)
    */
	// for more options that you may specify here, see the file: /wire/core/Process.php
	// and the file: /wire/core/Module.php

);